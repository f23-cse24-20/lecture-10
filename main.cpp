#include <iostream>
using namespace std;

int main() {

    string name, address1, address2, material;
    float length, width, height;
    bool delivery;

    getline(cin, name);
    getline(cin, address1);
    getline(cin, address2);
    cin >> length >> width >> height;
    cin.ignore();
    getline(cin, material);
    cin >> delivery;

    float materialPrice = 0;
    if (material == "Wood") {
        materialPrice = 1.5;
    }
    if (material == "Metal") {
        materialPrice = 2.0;
    }

    float wallPrice = ((length * height * 2) + (width * height * 2)) * materialPrice;

    float roofPrice = length * width * 3.0;

    float deliveryPrice = 0;
    if (delivery && (wallPrice + roofPrice) < 1500) {
        deliveryPrice = 100;
    }

    float totalPrice = wallPrice + roofPrice + deliveryPrice;

    cout << "-------------------------" << endl;
    cout << "          QUOTE          " << endl;
    cout << "-------------------------" << endl;
    cout << name << endl;
    cout << address1 << endl;
    cout << address2 << endl;
    cout << endl;

    cout << "DESCRIPTION SUMMARY" << endl;
    cout << "Dimensions: " << length << " x " << width << " x " << height << endl;
    cout << "Material: " << material << endl;

    if (delivery == true) {
        cout << "Delivery: Yes" << endl;
    } else {
        cout << "Delivery: No" << endl;
    }
    cout << endl;

    cout << "PRICE SUMMARY" << endl;
    cout << "Walls:    $" << wallPrice << endl;
    cout << "Roof:     $" << roofPrice << endl;
    cout << "Delivery: $" << deliveryPrice << endl;
    cout << "Total:    $" << totalPrice << endl;

    // command to create all quotes
    // for i in $(ls inquiries/); do cat inquiries/$i | ./app > quotes/quote-$i; done

    return 0;
}